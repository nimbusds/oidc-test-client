import java.io.*;
import java.net.*;

import javax.servlet.*;
import javax.servlet.http.*;

import net.minidev.json.*;

import com.thetransactioncompany.json.pretty.*;

import com.nimbusds.jose.crypto.*;
import com.nimbusds.jwt.*;

import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.auth.*;
import com.nimbusds.oauth2.sdk.http.*;
import com.nimbusds.oauth2.sdk.id.*;
import com.nimbusds.oauth2.sdk.token.*;
import com.nimbusds.openid.connect.sdk.*;
import com.nimbusds.openid.connect.sdk.claims.*;


/**
 * OpenID Connect login callback target.
 */
public class OIDCLoginCallback extends HttpServlet {


	@Override
	public void doGet(final HttpServletRequest req, final HttpServletResponse resp)
		throws ServletException, IOException {


		// *** *** *** Process the authorisation response *** *** *** //

		// Get the URL query string which contains the encoded 
		// authorisation response
		String queryString = req.getQueryString();

		PrintWriter out = resp.getWriter();

		out.println("URL query string with encoded authorization response: " + queryString + "\n\n");

		if (queryString == null || queryString.trim().isEmpty()) {

			out.println("Missing URL query string");
			return;
		}


		// Parse the authentication response
		AuthenticationResponse authResponse;

		try {
			authResponse = AuthenticationResponseParser.parse(new URL("http://?" + queryString));

		} catch (Exception e) {

			out.println("Couldn't parse OpenID Connect authentication response: " + e.getMessage());
			return;
		}

		if (authResponse instanceof AuthenticationErrorResponse) {

			// The authorisation response indicates an error, print
			// it and return immediately
			AuthenticationErrorResponse authzError = (AuthenticationErrorResponse)authResponse;
			out.println("Authentication error: " + authzError.getErrorObject());
			return;
		}

		// Authentication success, retrieve the authorisation code
		AuthenticationSuccessResponse authzSuccess = (AuthenticationSuccessResponse)authResponse;

		out.println("Authorization success:");
		out.println("\tAuthorization code: " + authzSuccess.getAuthorizationCode());
		out.println("\tState: " + authzSuccess.getState() + "\n\n");

		AuthorizationCode code = authzSuccess.getAuthorizationCode();

		if (code == null) {
			out.println("Missing authorization code");
			return;
		}



		// *** *** *** Make a token endpoint request *** *** *** //

		// Compose an access token request, authenticating the client
		// app and exchanging the authorisation code for an ID token
		// and access token
		URL tokenEndpointURL = new URL("http://localhost:8080/c2id/token");

		out.println("Sending access token request to " + tokenEndpointURL + "\n\n");

		// We authenticate with "client secret basic"
		ClientID clientID = new ClientID("test");
		Secret clientSecret = new Secret("7c840e40536b40ca801c80f0e7417951");
		ClientAuthentication clientAuth = new ClientSecretBasic(clientID, clientSecret);

		TokenRequest accessTokenRequest = new TokenRequest(
			tokenEndpointURL,
			clientAuth,
			new AuthorizationCodeGrant(code, new URL("http://localhost:8080/oidc-test-client/in"), clientID));

		HTTPRequest httpRequest;

		try {
			httpRequest = accessTokenRequest.toHTTPRequest();

		} catch (SerializeException e) {

			out.println("Couldn't create access token request: " + e.getMessage());
			return;
		}


		HTTPResponse httpResponse;

		try {
			httpResponse = httpRequest.send();

		} catch (IOException e) {

			// The URL request failed
			out.println("Couldn't send HTTP request to token endpoint: " + e.getMessage());
			return;
		}


		TokenResponse tokenResponse;

		try {
			tokenResponse = OIDCTokenResponseParser.parse(httpResponse);

		} catch (Exception e) {
			out.println("Couldn't parse token response: " + e.getMessage());
			return;
		}


		if (tokenResponse instanceof TokenErrorResponse) {

			// The token response indicates an error, print it out
			// and return immediately
			TokenErrorResponse tokenError = (TokenErrorResponse)tokenResponse;
			out.println("Token error: " + tokenError.getErrorObject());
			return;
		}


		OIDCAccessTokenResponse tokenSuccess = (OIDCAccessTokenResponse)tokenResponse;

		BearerAccessToken accessToken = (BearerAccessToken)tokenSuccess.getAccessToken();
		RefreshToken refreshToken = tokenSuccess.getRefreshToken();
		SignedJWT idToken = (SignedJWT)tokenSuccess.getIDToken();

		out.println("Token response:");

		out.println("\tAccess token: " + accessToken.toJSONObject().toString());
		out.println("\tRefresh token: " + refreshToken);
		out.println("\n\n");


		// *** *** *** Process ID token which contains user auth information *** *** *** //
		if (idToken != null) {
		
			out.println("ID token [raw]: " + idToken.getParsedString());

			out.println("ID token JWS header: " + idToken.getHeader());

			// Validate the ID token by checking its HMAC;
			// Note that PayPal HMAC generation is probably incorrect,
			// there's also a bug in the "exp" claim type
			try {
				MACVerifier hmacVerifier = new MACVerifier(clientSecret.getValue().getBytes());

				final boolean valid = idToken.verify(hmacVerifier);

				out.println("ID token is valid: " + valid);

				JSONObject jsonObject = idToken.getJWTClaimsSet().toJSONObject();

				out.println("ID token [claims set]: \n" + new PrettyJson().format(jsonObject));

				out.println("\n\n");

			} catch (Exception e) {

				out.println("Couldn't process ID token: " + e.getMessage());
			}
		}



		// *** *** *** Make a UserInfo endpoint request *** *** *** //

		// Note: The PayPal IdP uses an older OIDC draft version and
		// is at present not compatible with the Nimbus OIDC SDK so
		// we cannot use its helper call. We can however make a direct
		// call and simply display the raw data.

		URL userinfoEndpointURL = new URL("http://localhost:8080/c2id/userinfo/");

		// Append the access token to form actual request
		UserInfoRequest userInfoRequest = new UserInfoRequest(userinfoEndpointURL, accessToken);
		
		try {
			httpResponse = userInfoRequest.toHTTPRequest().send();

		} catch (Exception e) {

			// The URL request failed
			out.println("Couldn't send HTTP request to UserInfo endpoint: " + e.getMessage());
			return;
		}
		
		
		UserInfoResponse userInfoResponse;
		
		try {
			userInfoResponse = UserInfoResponse.parse(httpResponse);
			
		} catch (ParseException e) {
			
			out.println("Couldn't parse UserInfo response: " + e.getMessage());
			return;
		}
		
		
		if (userInfoResponse instanceof UserInfoErrorResponse) {
			
			out.println("UserInfo request failed");
			return;
		}
		
		
		UserInfo userInfo = ((UserInfoSuccessResponse)userInfoResponse).getUserInfo();
		

		out.println("UserInfo:");
		
		
		try {
			out.println(new PrettyJson().parseAndFormat(userInfo.toJSONObject().toString()));

		} catch (Exception e) {

			out.println("Couldn't parse UserInfo JSON object: " + e.getMessage());
		}
	}
}